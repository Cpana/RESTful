package eks.rest.client;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Konto {

    private int stand;
    private int nummer;
    private Kunde kunde;
    private ArrayList<Transaktion> transaktions;

    Konto(){transaktions = new ArrayList<>();}

    public ArrayList<Transaktion> getTransaktions() {
        return transaktions;
    }

    public void setTransaktions(ArrayList<Transaktion> transaktions) {
        this.transaktions = transaktions;
    }


    public void addTransaktion(Transaktion transaktion){
        transaktions.add(transaktion);
    }


    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

    public int getStand(){return stand;}
    public int getNummer(){return nummer;}

    public void setStand(int stand){this.stand = stand;}
    public void setNummer(int nummer){this.nummer  = nummer;}



}
