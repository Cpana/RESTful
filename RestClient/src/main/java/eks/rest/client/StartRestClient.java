package eks.rest.client;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Array;
import java.util.*;


public class StartRestClient {
  public static void main (String args[]) {
    Client client = ClientBuilder.newClient();  // Erzeugen eines Jersey-Client
    WebTarget target = client.target("http://localhost:55554/bankservices"); // Referenz auf eine Web-Ressource erzeugen

    /**1. Es sollen die Kunden „Peter“ mit Adresse „Bonn“, „Michael“ mit Adresse
     „Aachen“ und „Klaus“ mit Adresse „Paderborn“ angelegt werden.**/
    System.out.println("Aufgabe 1 Client Peter, Michael und Klaus anlegen ");
    Kunde peter = new Kunde();
    peter.setName("Peter");
    peter.setAdresse("Bonn");
    Response responsePeter = target.path("kunde").request().post(Entity.entity(peter, MediaType.APPLICATION_XML));
    System.out.println(responsePeter.toString());

    Kunde michael = new Kunde();
    michael.setName("Michael");
    michael.setAdresse("Aachen");
    Response responseMichael = target.path("kunde").request().post(Entity.entity(michael, MediaType.APPLICATION_XML));
    System.out.println(responseMichael.toString());

    Kunde klaus = new Kunde();
    klaus.setName("Klaus");
    klaus.setAdresse("Paderborn");
    Response responseKlausAbfrage = target.path("kunde").request().post(Entity.entity(klaus, MediaType.APPLICATION_XML));
    System.out.println(responseKlausAbfrage.toString());


    /**2. Es soll die Kunden-Ressource für den Kunden mit Namen „Peter“ erfragt
     werden und die Attribute der Rückgabe sollen ausgegeben werden.**/

    System.out.println("____________________________________________________");
    System.out.println("Aufgabe 2 Kunden-Ressource für Peter abfragen:");
    Kunde kundenabfrage = target.path("kunde").path("Peter").request().accept(MediaType.APPLICATION_XML).get(Kunde.class);
    System.out.println("Name:" + kundenabfrage.getName() + " Adresse: " + kundenabfrage.getAdresse());
    System.out.println("____________________________________________________");

    /**3. Es soll für jeden Kunden jeweils 1 Konto eingerichtet werden mit dem
     Kontostkundenabfrageand 50€ für „Peter“, 80€ für „Michael“ und peter120€ für „Klaus“. Es sollen
     die vergebenen Kontonummern ausgegeben werden.**/
    System.out.println("Aufgabe 3. Konto fuer Peter, Michael und Klaus anlegen (Ausgabe Status und localhost Link)");
    Konto neuesKontoPeter = new Konto();
    neuesKontoPeter.setStand(50);
    neuesKontoPeter.setKunde(peter);

    Response peterKontoResponse = target.path("kunde/Peter/konto").request().post(Entity.entity("50", MediaType.TEXT_PLAIN));
    System.out.println(peterKontoResponse.toString());
    System.out.println("Vergebene Kontonummer : " + peterKontoResponse.readEntity(String.class));


    Response michaelKontoResponse = target.path("kunde/Michael/konto").request().post(Entity.text("80"));
    String kontonummerMichael = michaelKontoResponse.readEntity(String.class);
    System.out.println(michaelKontoResponse.toString());
    System.out.println("Vergebene Kontonummer : " + kontonummerMichael);

    Response klausKontoResponse = target.path("kunde/Klaus/konto").request().post(Entity.text(120));
    System.out.println(klausKontoResponse.toString());
    System.out.println("Vergebene Kontonummer : " + klausKontoResponse.readEntity(String.class));

      /*
          4. Auf das Konto von „Michael“ sollen 30€ eingezahlt werden. Die Daten der
          Rückgabe sollen ausgegeben werden.
      */
    System.out.println("____________________________________________________");
    System.out.println("Aufgabe 4. Auf Michaels Konto sollen 30 euro Eingezahlt werden");
    Transaktion transaktion = new Transaktion();
    transaktion.setTyp(TransaktionType.EINZAHLEN);
    System.out.println(transaktion.getTyp().toString());
    transaktion.setBetrag(30);
    transaktion.setKonto(2);
    Response einzahlenMichael = target.path("kunde/Michael/konto/2/transaktion").request().post(Entity.entity(transaktion, MediaType.APPLICATION_XML));
    System.out.println(einzahlenMichael.toString());
    System.out.println("Konto von : " + einzahlenMichael.readEntity(Konto.class).getStand());
    System.out.println("____________________________________________________");


    /*
     * 5. Es sollen alle Konten von „Michael“ erfragt werden und die jeweiligen
     *Kontonummern und Kontostände ausgegeben werden.
     * */

    System.out.println("Aufgabe 5 Abfrage aller Konten von Michael");
    Konto[] alleKontenVonMichael = target.path("kunde/Michael/konto").request().accept(MediaType.APPLICATION_XML).get(Konto[].class);
    for (Konto konto : alleKontenVonMichael)
      System.out.println("Konto von :" + konto.getKunde().getName() + "  Kontonummer:" + konto.getNummer() + "Kontostand : " + konto.getStand());
      /*
      * 6. Von dem Konto von „Michael“ sollen 20€ abgehoben werden. Die Daten der
        Rückgabe sollen ausgegeben werden.
      * */
    System.out.println("____________________________________________________");
    System.out.println("Aufgabe 6 Abheben von 20 euro ");
    Transaktion transaktion1 = new Transaktion();
    transaktion1.setTyp(TransaktionType.AUSZAHLEN);
    transaktion1.setBetrag(20);
    transaktion1.setKonto(2);
    Response michaelResponseAuszahlung = target.path("kunde/Michael/konto/2/transaktion").request().post(Entity.entity(transaktion1, MediaType.APPLICATION_XML));
    System.out.println(michaelResponseAuszahlung.toString());

    System.out.println("____________________________________________________");



    /*
     *7. Es soll für das Konto von „Michael“ der aktuelle Kontostand erfragt und ausgegeben werden.
     */

    System.out.println("Aufgabe 7");
    Response kontoAbfrageMichael = target.path("kunde/Michael/konto/"+kontonummerMichael+"/stand").request().accept(MediaType.TEXT_PLAIN).get();
    System.out.println(kontoAbfrageMichael.toString());
    System.out.println("Kontostand von Michael" + kontoAbfrageMichael);
    System.out.println("____________________________________________________");

    /*
     *   8. Für das Konto von „Michael“ sollen alle Einzahlungen abgefragt und die Daten
     *   der Rückgabe ausgegeben werden.
     * */

    System.out.println("Aufgabe 8");
    Transaktion[] kontoEinzahlungAbfrageMichael = target.path("kunde/Michael/konto/2/transaktion")
            .queryParam("typ", TransaktionType.EINZAHLEN).request().accept(MediaType.APPLICATION_XML).get(Transaktion[].class);
    for (Transaktion t : kontoEinzahlungAbfrageMichael) {
        System.out.println(t.getTyp().toString() + t.getBetrag());
    }


    System.out.println("____________________________________________________");
    /*
     * 9. Für das Konto von „Michael“ sollen alle Auszahlungen abgefragt und die Daten
     *  der Rückgabe ausgegeben werden.
     * */
    System.out.println("Aufgabe 9");
    Transaktion[] kontoAuszahlenAbfrageMichael = target.path("kunde/Michael/konto/{id}/transaktion").resolveTemplate("id", kontonummerMichael).
            queryParam("typ", TransaktionType.AUSZAHLEN).request().accept(MediaType.APPLICATION_XML).get(Transaktion[].class);
    for (Transaktion t2 : kontoAuszahlenAbfrageMichael) {
        System.out.println(t2.getTyp().toString() + t2.getBetrag());
    }

    System.out.println("____________________________________________________");

    /*  10.
     *  Es soll die Liste aller Kunden-Objekte im Format XML erfragt werden können.
     *  (Auf alle Ressourcen eines Containers zugreifen)
     */

    System.out.println("Aufgabe 10");
    int zähler = 0;
    List<Kunde> alleRessourcen = target.path("kunde").request().accept(MediaType.APPLICATION_XML).get(new GenericType<List<Kunde>>(){});
    for(Kunde k : alleRessourcen) {
      System.out.println(k.getName());
    }
  }
}

