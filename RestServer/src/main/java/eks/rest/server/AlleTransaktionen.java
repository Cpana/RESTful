package eks.rest.server;

import java.util.ArrayList;
import java.util.List;

public class AlleTransaktionen {

    private static AlleTransaktionen instance;
    private ArrayList<Transaktion> transaktions;

    public static int getTransId() {
        return transId;
    }

    private static int transId = 1;

    private AlleTransaktionen() {
        transaktions = new ArrayList<>();
    }

    public static AlleTransaktionen getInstance() {
        if (AlleTransaktionen.instance == null) {
            AlleTransaktionen.instance = new AlleTransaktionen();
        }
        return AlleTransaktionen.instance;
    }

    public Transaktion getTransaktion(int transActionId) {
        for (Transaktion transaktion : transaktions) {
            if (transaktion.getTransId() == transActionId)
                return transaktion;
        }
        return null;
    }

    public void addTransaktion(Transaktion neueTransaktion) {
        transaktions.add(neueTransaktion);
        transId++;
    }

    public ArrayList<Transaktion> getTransaktionByType(TransaktionType type) {
        ArrayList<Transaktion> gesuchterTyp = new ArrayList<>();
        if (type.equals(TransaktionType.EINZAHLEN)) {
            for (Transaktion t : transaktions) {
                if (t.getTyp().toString().equals(type.toString())) {
                    gesuchterTyp.add(t);
                }
            }
            return gesuchterTyp;
        } else {
            for (Transaktion t : transaktions) {
                if (t.getTyp().toString().equals(type.toString())) {
                    gesuchterTyp.add(t);
                }
            }
            return gesuchterTyp;
        }
    }
}