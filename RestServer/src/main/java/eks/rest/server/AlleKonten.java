package eks.rest.server;

import java.util.ArrayList;

public class AlleKonten {

    private static AlleKonten instance;
    private ArrayList<Konto> konten;
    private static int id = 0;

    public static int getId() {
        return id;
    }



    private AlleKonten(){
        konten = new ArrayList<>();
    }

    public static AlleKonten getInstance(){
        if(AlleKonten.instance == null){
            AlleKonten.instance = new AlleKonten();
        }
        return AlleKonten.instance;
    }  ;


    void addKonto(Konto neuesKonto){
        konten.add(neuesKonto);
        id++;

    }

    public ArrayList<Konto> getKonten(){
        return konten;
    }


    public Konto getKonto(int kontoNummer) {
        for(Konto k: konten){
            if(k.getNummer()==kontoNummer){
                return k;
            }
        }
        return null;
    }
}
