package eks.rest.server;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Transaktion {


    private TransaktionType ein_aus;
    private int betrag;
    private int Konto;

    public int getTransId() {
        return transId;
    }

    private int transId;

    public int getKonto() {
        return Konto;
    }

    public void setKonto(int konto) {
        Konto = konto;
    }

    public TransaktionType getTyp() {
        return ein_aus;
    }

    public void setTyp(TransaktionType typ) {
        this.ein_aus = typ;
    }

    public int getBetrag() {
        return betrag;
    }

    public void setBetrag(int betrag) {
        this.betrag = betrag;
    }

}