package eks.rest.server;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;

@Path("/")
public class Services {


    /**1. Erzeugung eines neuen Kunden. Hierbei werden der Name und die Adresse des
     neuen Kunden in XML angegeben. (Eine neue Ressource erzeugen)**/
    @POST
    @Path("/kunde")
    @Consumes("application/xml")
    public void neuerKunde(Kunde neuerKunde){
        AlleKunden alleKunden = AlleKunden.getInstance();
        alleKunden.addKunde(neuerKunde);
    }


    /**2. Abfrage der Kunden-Ressource für einen Kunden mit gegebenem Namen. Als
     Rückgabe soll eine XML-Beschreibung der Ressource geliefert werden. (Eine
     einzelne Ressource adressieren)**/


    @GET
    @Path("/kunde/{kundenname}")
    @Produces("application/xml")
    public Kunde getKunde(@PathParam("kundenname") String kundenName){
        AlleKunden alleKunden = AlleKunden.getInstance();
        Kunde rueckgabe = alleKunden.getKunde(kundenName);
        return rueckgabe;
    }

    /**3. Für einen Kunden mit gegebenem Namen soll ein neues Konto erstellt werden. Es
     wird hierbei der gewünschte Kontostand des neuen Kontos als String übergeben.
     Ein Kunde kann mehrere Konten besitzen. Der Dienst liefert die Nummer des neu
     angelegten Kontos als String zurück. Eine Kontonummer muss innerhalb der Bank
     eindeutig sein. (Eine neue Ressource über eine Assoziation erstellen)**/



    @POST
    @Path("kunde/{kundenname}/konto")
    @Produces("text/plain")
    public String neuesKonto(@PathParam("kundenname") String kundenName, String kontostand){

        AlleKunden alleKunden = AlleKunden.getInstance();
        Kunde gesuchterKunde = alleKunden.getKunde(kundenName);

        AlleKonten alleKonten = AlleKonten.getInstance();

        Konto neuesKonto = new Konto();
        neuesKonto.setKunde(gesuchterKunde);
        neuesKonto.setNummer(alleKonten.getId()+1);
        neuesKonto.setStand(Integer.parseInt(kontostand));
        gesuchterKunde.addKonto(neuesKonto);
        alleKonten.addKonto(neuesKonto);


        String kontonummer = String.valueOf(neuesKonto.getNummer());
        return kontonummer;
    }

    /*4. Für ein Konto migegebener Kontonummer soll eine neue Transaktion angelegt
        werden (d.h. Einzahlung oder Auszahlung) und der Kontostand des Kontos soll
        entsprechend dieser Transaktion angepasst werden. Es wird eine Antwort im
        XML-Format mit den folgenden Angaben geliefert: Name des Kunden, neuer
        Kontostand. Übergabe an den Server soll hierbei eine XML-Beschreibung der
        Transaktion sein. (Eine neue Ressource über eine Assoziation erstellen)
        */

    @POST
    @Path("kunde/{kundenname}/konto/{id}/transaktion")
    @Consumes(MediaType.APPLICATION_XML)
    //@Produces(MediaType.APPLICATION_XML)
    public Konto transaktion(@PathParam("kundenname") String kundenName, @PathParam("id")String id , Transaktion transaktion){
        AlleKonten alleKonten = AlleKonten.getInstance();
        Konto kontoKunde = alleKonten.getKonto(Integer.parseInt(id));
        AlleTransaktionen alleTransaktionen = AlleTransaktionen.getInstance();

        if(transaktion.getTyp().toString() == "EINZAHLEN"){
            Transaktion transaktion1 = new Transaktion();
            transaktion1.setKonto(kontoKunde.getNummer());
            transaktion1.setBetrag(transaktion.getBetrag());
            transaktion1.setTyp(transaktion.getTyp());
            kontoKunde.setStand(kontoKunde.getStand() + transaktion.getBetrag());
            kontoKunde.addTransaktion(transaktion1);
            alleTransaktionen.addTransaktion(transaktion);
            return kontoKunde;
        }else if(transaktion.getTyp().toString() == "AUSZAHLEN"){
            Transaktion transaktion1 = new Transaktion();
            transaktion1.setKonto(kontoKunde.getNummer());
            transaktion1.setBetrag(transaktion.getBetrag());
            transaktion1.setTyp(transaktion.getTyp());
            kontoKunde.setStand(kontoKunde.getStand()-transaktion.getBetrag());
            kontoKunde.addTransaktion(transaktion1);
            alleTransaktionen.addTransaktion(transaktion);
            return kontoKunde;
        }
        return null;
    }

    /**5. Für einen Kunden mit einem gegebenen Namen soll die Liste aller Konten-Objekte
     des Kunden im Format XML geliefert werden. (Verfolgung einer Assoziation)**/
    @Path("kunde/{kundenname}/konto")
    @GET
    @Produces("application/xml")
    public Konto[] getKonten (@PathParam("kundenname") String name){
        AlleKunden alleKunden = AlleKunden.getInstance();
        Kunde kunde = alleKunden.getKunde(name);
        ArrayList<Konto>konten = kunde.getKonten();
        Konto[] rueckgabe = konten.toArray(new Konto[0]);
        return rueckgabe;
    }

    /**6. Für ein Konto mit gegebener Kontonummer soll der aktuelle Kontostand erfragt
     werden. Der Kontostand wird vom Server als String geliefert. (Projektion)**/
    @Path("kunde/{kundenname}/konto/{id}/stand")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getKontostand(@PathParam("id")String Kontonummer){
        AlleKonten alleKonten = AlleKonten.getInstance();
        Konto konto = alleKonten.getKonto(Integer.parseInt(Kontonummer));
        String rueckgabe = String.valueOf(konto.getStand());
        return rueckgabe;
    }

    /**7. Für ein Konto mit gegebener Kontonummer sollen alle Einzahlungen bzw. alle
     Auszahlungen abgefragt werden können. Rückgabe ist die Menge an
     entsprechenden Transaktionen in XML. (Selektion)**/
    @Path("kunde/{kundenname}/konto/{id}/transaktion")
    @GET
    @Produces("application/xml")
    public Transaktion[] getTransaktion (@PathParam("id") String id,
                                         @QueryParam("typ")TransaktionType type){

        AlleKonten alleKonten = AlleKonten.getInstance();
        Konto konto = alleKonten.getKonto(Integer.parseInt(id));
        AlleTransaktionen alleTransaktionen = AlleTransaktionen.getInstance();
        ArrayList<Transaktion>transaktions;
        if(type == null){
             transaktions = konto.getTransaktions();
        }else{
            transaktions = alleTransaktionen.getTransaktionByType(type);
        }
        Transaktion[] rueckgabe = transaktions.toArray(new Transaktion[0]);
        return rueckgabe;
    }


    /**8. Es soll die Liste aller Kunden-Objekte im Format XML erfragt werden können. (Auf
     alle Ressourcen eines Containers zugreifen)**/
    @Path("/kunde")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getKunden(){
        AlleKunden alleKunden = AlleKunden.getInstance();
        List<Kunde>kunden = alleKunden.getKunden();
        GenericEntity<List<Kunde>> genericEntity = new GenericEntity<List<Kunde>>(kunden){};
        return Response.ok(genericEntity).build();
    }


}