package eks.rest.server;

import java.util.ArrayList;

public class AlleKunden {

    private static AlleKunden instance;

    private ArrayList<Kunde> kunden;

    private AlleKunden(){
        kunden = new ArrayList<>();
    }

    public static AlleKunden getInstance(){
        if(AlleKunden.instance == null){
            AlleKunden.instance = new AlleKunden();
        }
        return AlleKunden.instance;
    }

    void addKunde(Kunde neuerKunde){ kunden.add(neuerKunde);
    }

    public ArrayList<Kunde> getKunden() {
        return kunden;
    }


    public Kunde getKunde(String kundeName) {
        for(Kunde k: kunden){
            if(k.getName().equals(kundeName)){
                return k;
            }
        }
        return null;
    }
}