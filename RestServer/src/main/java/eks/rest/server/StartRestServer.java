package eks.rest.server;


import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.glassfish.jersey.server.ResourceConfig;
import javax.swing.JOptionPane;

public class StartRestServer
{ 
 public static void main (String args[]) throws Exception {

    URI endpoint = new URI("http://localhost:55554/bankservices");

    ResourceConfig rc = new ResourceConfig(Services.class);
    HttpServer server =  GrizzlyHttpServerFactory.createHttpServer( endpoint, rc);
  }
}
