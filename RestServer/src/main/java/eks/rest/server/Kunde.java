package eks.rest.server;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;


@XmlRootElement
public class Kunde {

    private String name;
    private String adresse;

    @XmlTransient
    private ArrayList<Konto> konten;

    Kunde(){konten = new ArrayList<>();}

    public void addKonto(Konto k){ konten.add(k); }

    @XmlTransient
    public ArrayList<Konto> getKonten(){ return konten;}

    public Konto getKonto(int kontoNummer){
        for(Konto k: konten){
            if(k.getNummer()==kontoNummer){
                return k;
            }
        }
        return null;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getAdresse() { return adresse; }

    public void setAdresse(String adresse) { this.adresse = adresse; }


}
